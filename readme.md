# Pan Tilt Head

![DSCN3339.JPG](https://bitbucket.org/repo/qenLdy/images/51239531-DSCN3339.JPG)

## Parts used

Most/all of the stuff used is the cheapest things that you can easily find on ebay for a couple of $$$.

* Stepper motor (28BYJ-48) with a ULN2003 driver board
* 2N2222 transistor with a 10k resistor for the shutter release (with a 2.5mm jack to connect to the camera)

#### Classic version
* Standard 20x4 characters LCD
* Serial Interface Board Module for LCD (This allow to control the LCD via I2C)
* 4x4 Keypad
* Arduino Uno

#### Bluetooth version
* Serial Bluetooth Transceiver Module HC-05
* Arduino Nano

## External libraries used

Additional libraries needs to be added to Documents/Arduino/libraries/

* [ AccelStepper ](http://www.pjrc.com/teensy/td_libs_AccelStepper.html)
* [ TimerOne ](http://www.pjrc.com/teensy/td_libs_TimerOne.html)

#### Classic version
* [ LiquidCrystal_I2C ](https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home)
* [ Keypad ](http://www.pjrc.com/teensy/td_libs_Keypad.html)

#### Bluetooth version
* [ ArduinoJson ](https://github.com/bblanchon/ArduinoJson)

### Manual library installations

```
cd <your arduino sketch folder>/libraries
wget http://www.pjrc.com/teensy/arduino_libraries/AccelStepper-1.30.zip
wget https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/LiquidCrystal_V1.2.1.zip
wget http://www.pjrc.com/teensy/arduino_libraries/TimerOne.zip
wget http://www.pjrc.com/teensy/arduino_libraries/Keypad.zip
wget https://github.com/bblanchon/ArduinoJson/archive/master.zip
for i in *.zip ; do unzip $i ; done
rm -fr __MACOSX
```

## Classic version

This version of the remote is using a keypad and a LCD to programme the Timelapse settings.

#### The running process for Timelapse

To track time, the var `waitCounter100ms` is used. There is a counter under interruptions decremented this variable every 100ms. This can go in negative, and will go in negative when the platform is moving. This will then be subtracted from the programmed inter-frame time in the state `RUNNING_PRE_WAIT`.

* `RUNNING_PRE_WAIT` : Wait for the programmed inter-frame delay. This will automatically subtract the time spent during the moving process has explained before.
* `RUNNING_SHUTTER_RELEASE` : Release the shutter of the camera
* `RUNNING_POST_WAIT` : Wait for the programmed shutter speed. The entered shutter speed value will be saved in a 0.1s format (1/10 will be saved as 1), rounded to the closest integer (1/60 will be 0, 1/15 will be 1). And this value will be incremented by the defined value `SHUTTERSPEED_MARGIN_100MS` (currently 1, meaning +100ms)
* `RUNNING_MOVE` : Move the platform according to the settings

## Bluetooth version

This version of the remote is using a Bluetooth module to communicate with a phone to programme the Timelapse settings. 

Anyone can develop a compatible App, you just need to conforme to the JSON spec (Bluetooth/JSON_spec.txt).
Here is the list of known projects that are compatible with the board :

* [ Android App developed by me ](https://bitbucket.org/xantra/timelapseremote)
* Android App developed by the spec owner (link to be added)

#### The running process for Timelapse

There is 2 steps managed under interruptions. Waiting and Moving. Here is all the steps in order that the program is going through in case of a timelaspe.

* `active_shutter` : Release the shutter of the camera.
* `active_wait` : Wait for the programmed `intershot_delay`. An interruption is triggered every `WAIT_REFRESH_TIME_US` micro-seconds, each time decrementing `wait_counter`.
* `active_motor` : In case of a moving timelapse, move the platform according to the settings. Again this is under interruptions, triggered every `MOTOR_REFRESH_TIME_US` micro-seconds. 

The shutterspeed isn't taken into account yet, that's why you need to make sure that your interframe delay is a bit bigger than your shutterspeed.