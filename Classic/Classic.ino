
#include <AccelStepper.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include <TimerOne.h>


#define SHUTTERSPEED_MARGIN_100MS 1 // This is adding 100ms to the shutter release time
#define nbStepsInOnePanRevolution 37250 // 64*64*128/14 (-200, error correction)

#define shutterReleasePin A0

#define LCD_DEVICEID 0x27
// Connecter to I2C: A4 (SDA) and A5 (SCL) on Arduino board

#define LCD_MENU_INIT_1 "Timelapse   A:Static"
#define LCD_MENU_INIT_2 "  Pan B:Right C:Left"
#define LCD_MENU_INIT_3 "D:PanoPhoto         "
#define LCD_MENU_INIT_4 "1/3: 360     4/6: 1 "

#define LCD_MENU_SETUP_NBSHOT_1 "Nb photos :         "
#define LCD_MENU_SETUP_NBSHOT_2 "Final 25fps:        "
#define LCD_MENU_SETUP_NBSHOT_3 "                    "
#define LCD_MENU_SETUP_NBSHOT_4 "  A:Del       #:OK  "

#define LCD_MENU_SETUP_PANNING_1 "Nb steps :          "
#define LCD_MENU_SETUP_PANNING_2 "Full angle :        "
#define LCD_MENU_SETUP_PANNING_3 "  B:Demo     C:Swap "
#define LCD_MENU_SETUP_PANNING_4 "  A:Del       #:OK  "

#define LCD_MENU_SETUP_INTERSHOT_DELAY_1 "Interval in s :     "
#define LCD_MENU_SETUP_INTERSHOT_DELAY_2 "Will take 0:00:00:00"
#define LCD_MENU_SETUP_INTERSHOT_DELAY_3 "                    "
#define LCD_MENU_SETUP_INTERSHOT_DELAY_4 "  A:Del       #:OK  "

#define LCD_MENU_SETUP_SHUTTER_1 "Shutter in s : xxxxx"
#define LCD_MENU_SETUP_SHUTTER_2 "                    "
#define LCD_MENU_SETUP_SHUTTER_3 "  B: >1s or <1s     "
#define LCD_MENU_SETUP_SHUTTER_4 "  A:Del       #:OK  "

#define LCD_MENU_READY_TO_GO_1 "                    "
#define LCD_MENU_READY_TO_GO_2 "      Ready ?       "
#define LCD_MENU_READY_TO_GO_3 "      # : GO        "
#define LCD_MENU_READY_TO_GO_4 "                    "


// Panning Motor pin definitions
#define motorPanPin1  2     // IN1 on the ULN2003 driver 1
#define motorPanPin2  3     // IN2 on the ULN2003 driver 1
#define motorPanPin3  4     // IN3 on the ULN2003 driver 1
#define motorPanPin4  5     // IN4 on the ULN2003 driver 1

// Keypad settings
#define KEYPAD_ROWS 4 //four rows
#define KEYPAD_COLS 4 //four columns
char keypadKeys[KEYPAD_ROWS][KEYPAD_COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}};
byte keypadRowPins[KEYPAD_ROWS] = {13, 12, 11, 10}; //connect to the row pinouts of the keypad
byte keypadColPins[KEYPAD_COLS] = {9, 8, 7, 6};     //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keypadKeys), keypadRowPins, keypadColPins, KEYPAD_ROWS, KEYPAD_COLS );

// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper with 28BYJ-48
AccelStepper panning(AccelStepper::HALF4WIRE, motorPanPin1, motorPanPin3, motorPanPin2, motorPanPin4);

LiquidCrystal_I2C lcd(LCD_DEVICEID, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

#define NB_CUSTOM_CHAR 1
#define CUSTOM_CHAR_DEGRE char(0)
const uint8_t customChar[NB_CUSTOM_CHAR][8] = {
   { 0xc, 0x12, 0x12, 0xc, 0, 0, 0, 0 } 
};


char keyPressed = NO_KEY;

enum state { INIT, SETUP_NBSHOT, SETUP_PANNING, SETUP_INTERSHOT_DELAY, SETUP_SHUTTER, READY_TO_GO, RUNNING_PRE_WAIT, RUNNING_SHUTTER_RELEASE, RUNNING_POST_WAIT, RUNNING_MOVE };
state currentState = INIT;
void changeState(state newState); // I have no idea why I need to do that, but it's required. Maybe something linked to the enum type


struct typingZone {
  bool active = false;
  unsigned int activeChar;
  unsigned int lineNumber;
  unsigned int firstCol;
  unsigned int nbCol;
  unsigned long currentValue;
};
typingZone * typingZoneCollection[10];
unsigned int nbTypingZone;

bool panningDirection;
bool noPanning;

unsigned int nbPhotoTypingArea, setupPanningTypingArea, intervalTypingArea, shutterspeedTypingArea;
unsigned long nbPhotos, nbSteps, interval, shutterspeed_100ms;
unsigned long nbPhotoToTake;

long waitCounter100ms;

bool shutterspeedFraction;
bool panningSetupInSteps;


void timer100msInterrupt() {
  waitCounter100ms--;
}

void cleanup() ; // defined near the end...

void setup() {
  Serial.begin(9600);
  
  pinMode(shutterReleasePin, OUTPUT);
  digitalWrite(shutterReleasePin, LOW);
  
  Timer1.initialize(100000); // Init the timer 1 to a 100ms period
  Timer1.stop();
  Timer1.attachInterrupt(timer100msInterrupt);
  
  cleanup();
  
  lcd.begin(20,4);               // initialize a 20x4 characters lcd 
  for ( int i = 0; i < NB_CUSTOM_CHAR; i++ )
    lcd.createChar ( i, (uint8_t *)customChar[i] );
  
  panning.setMaxSpeed(1000.0);
  panning.setAcceleration(500.0); // acceleration/deceleration rate
  panning.runToNewPosition(64); // reset motor position
   
  printFullScreen();
}


// Disable a typing area and enable another one
// Don't enable anything if parameter is < 0
void changeTypingArea(int newArea) {
  for (unsigned int i = 0; i < nbTypingZone; i++) 
    typingZoneCollection[i]->active = false;

  if (newArea >= 0 && newArea < nbTypingZone) {
    typingZone * currentTypingZone = typingZoneCollection[newArea];
    currentTypingZone->active = true;    
  }
}

// Reset the typing area
void resetTypingArea(int index) {
  typingZone * currentTypingZone = typingZoneCollection[index];
  
  currentTypingZone->activeChar = 0;
  currentTypingZone->currentValue = 0;
  
  lcd.setCursor(currentTypingZone->firstCol, currentTypingZone->lineNumber);
  for (unsigned int i = 0; i < currentTypingZone->nbCol; i++){
    lcd.print('_'); 
  }
}

void resetCurrentTypingArea() {
  for (unsigned int i = 0; i < nbTypingZone; i++) { // Looking for the current active area
    if (typingZoneCollection[i]->active) {
      resetTypingArea(i);
      return;
    }
  }
}

// Init an area on the screen where the user will be able to dia numbers
// Return the index where is stored the object created to manage this area
// The created area is active by default
int initTypingZone(int lineNumber, int firstCol, int nbCol) {
  
  typingZone * newTypingZone = new typingZone;
  newTypingZone->lineNumber = lineNumber;
  newTypingZone->firstCol = firstCol;
  newTypingZone->nbCol = nbCol;
  
  typingZoneCollection[nbTypingZone++] = newTypingZone;
  
  resetTypingArea(nbTypingZone-1);
  changeTypingArea(nbTypingZone-1);
  
  return nbTypingZone-1;
}

// Give a typed key as a parameter, process the typing area to keep everything up to date, and also update the screen
void processTypingArea(char typedChar) {
  if (typedChar < '0' || typedChar > '9') // If not a number, discare
    return;
  
  for (unsigned int i = 0; i < nbTypingZone; i++) { // Looking for the current active area
    typingZone * currentTypingZone = typingZoneCollection[i];
    if (currentTypingZone->active) {
      if (currentTypingZone->activeChar == currentTypingZone->nbCol) // If no free space left, discare
        return;

      lcd.setCursor(currentTypingZone->firstCol + currentTypingZone->activeChar, currentTypingZone->lineNumber);      
      lcd.print(typedChar);

      currentTypingZone->currentValue *= 10; // Shift the current value
      currentTypingZone->currentValue += (typedChar - '0'); // Add the new value
      
      currentTypingZone->activeChar++;
    }
  }
}

// Get the value in the typing specified area
unsigned long getTypingAreaValue(int index) {
  return typingZoneCollection[index]->currentValue;
}


void printFullScreenHelper(String line0, String line1, String line2, String line3) {
  lcd.print(line0);
  lcd.setCursor(0,1);
  lcd.print(line1);
  lcd.setCursor(0,2);
  lcd.print(line2);
  lcd.setCursor(0,3);
  lcd.print(line3);  
}

void printFullScreen() {
  lcd.home();
  switch (currentState) {
    case INIT:
      printFullScreenHelper(LCD_MENU_INIT_1, LCD_MENU_INIT_2, LCD_MENU_INIT_3, LCD_MENU_INIT_4);
      lcd.setCursor(8, 3);
      lcd.print(CUSTOM_CHAR_DEGRE);
      lcd.setCursor(19, 3);
      lcd.print(CUSTOM_CHAR_DEGRE);
    break;

    case SETUP_NBSHOT:
      printFullScreenHelper(LCD_MENU_SETUP_NBSHOT_1, LCD_MENU_SETUP_NBSHOT_2, LCD_MENU_SETUP_NBSHOT_3, LCD_MENU_SETUP_NBSHOT_4);
    break;
     
    case SETUP_PANNING:
      printFullScreenHelper(LCD_MENU_SETUP_PANNING_1, LCD_MENU_SETUP_PANNING_2, LCD_MENU_SETUP_PANNING_3, LCD_MENU_SETUP_PANNING_4);   
    break;
     
    case SETUP_INTERSHOT_DELAY:
      printFullScreenHelper(LCD_MENU_SETUP_INTERSHOT_DELAY_1, LCD_MENU_SETUP_INTERSHOT_DELAY_2, LCD_MENU_SETUP_INTERSHOT_DELAY_3, LCD_MENU_SETUP_INTERSHOT_DELAY_4); 
    break;
    
    case SETUP_SHUTTER:
      printFullScreenHelper(LCD_MENU_SETUP_SHUTTER_1, LCD_MENU_SETUP_SHUTTER_2, LCD_MENU_SETUP_SHUTTER_3, LCD_MENU_SETUP_SHUTTER_4);    
    break;

    case READY_TO_GO:
      printFullScreenHelper(LCD_MENU_READY_TO_GO_1, LCD_MENU_READY_TO_GO_2, LCD_MENU_READY_TO_GO_3, LCD_MENU_READY_TO_GO_4);
    break;
    
    case RUNNING_PRE_WAIT:
      lcd.clear();
      lcd.print("Nb photos: ");
      lcd.setCursor(0, 1);
      if (!noPanning) {
        lcd.print("Nb steps: ");
        lcd.print(nbSteps);
        lcd.setCursor(0, 2);
      }
      lcd.print("Interval: ");
      lcd.print(interval);
    break;
    
    default:
    break;
  }
}

// Refresh some computed values
void refreshLiveData() {
  double angle, finalAt25fps;
  unsigned long nbSeconds;
  unsigned int days, hours, minuts, seconds;
  
  switch (currentState) {
    
    case SETUP_NBSHOT:
      lcd.setCursor(13, 1);
      lcd.print("       ");

      nbPhotos = getTypingAreaValue(nbPhotoTypingArea);
      if (nbPhotos != 0) {
        finalAt25fps = nbPhotos / 25.0;
        lcd.setCursor(13, 1);
        if (finalAt25fps < 100)
          lcd.print(finalAt25fps);
        else
          lcd.print((long)finalAt25fps);
        lcd.print('s');
      }
    break;
    
    case SETUP_PANNING:
      if (panningSetupInSteps) {
        lcd.setCursor(13, 1);
        lcd.print("       ");
  
        nbSteps = getTypingAreaValue(setupPanningTypingArea);
        if (nbSteps != 0) {
          angle = (360.00 / nbStepsInOnePanRevolution) * nbPhotos * nbSteps;
          lcd.setCursor(13, 1);
        
          // Now write the computer angle
          if (angle < 360)
            lcd.print(angle);
          else
            lcd.print((long)angle);
          lcd.print(CUSTOM_CHAR_DEGRE);
        }
      } else {
        lcd.setCursor(11, 1);
        lcd.print("         ");
        
        angle = getTypingAreaValue(setupPanningTypingArea);
        if (angle != 0) {
          nbSteps = (nbStepsInOnePanRevolution / 360.00) * angle / nbPhotos;
          lcd.setCursor(11, 1);
          lcd.print(nbSteps);
        } 
      }
    break;
     
    case SETUP_INTERSHOT_DELAY:
      interval = getTypingAreaValue(intervalTypingArea);
      
      if (interval != 0) {
        unsigned long buff;
        
        nbSeconds = interval * nbPhotos;
        
        seconds = nbSeconds%60;
        buff = nbSeconds/60;
        
        minuts = buff%60;
        buff /= 60;
        
        hours = buff%24;
        days = buff/24;
      } else {
        hours = 0;
        minuts = 0;
        seconds = 0;
        days = 0;
      }
      
      // Now write the computer time
      lcd.setCursor(9, 1);
      if (days < 10)
        lcd.print(' ');
      lcd.print(days); 

      lcd.setCursor(12, 1);      
      if (hours < 10)
        lcd.print('0');
      lcd.print(hours); 
      
      lcd.setCursor(15, 1);
      if (minuts < 10)
        lcd.print('0');
      lcd.print(minuts);  
     
      lcd.setCursor(18, 1);
      if (seconds < 10)
        lcd.print('0');
      lcd.print(seconds);  

    break;
    
    case SETUP_SHUTTER:
      if (shutterspeedFraction)
        shutterspeed_100ms = (long)(10.0/getTypingAreaValue(shutterspeedTypingArea)); // Round it
      else
        shutterspeed_100ms = 10*getTypingAreaValue(shutterspeedTypingArea);
    break;
    
    case RUNNING_PRE_WAIT:
      lcd.setCursor(11, 0);
      lcd.print(nbPhotoToTake - nbPhotos + 1);
      lcd.print("/");
      lcd.print(nbPhotoToTake);      
    break;
    
    default:
    break;
  }
}

// TODO
void runPanDemo() {
  panning.runToNewPosition(1 * (panningDirection * -1));
}

void progressBar(bool init = false) {
  static int increment = 0;
  static int stage = 0;
  static int nbIncrementByStage;
  
  increment++;
    
  if (init) {
    stage = 0;
    increment = 1;

    lcd.setCursor(0, 3);
    lcd.print("[                  ]");
    lcd.setCursor(1, 3);
    nbIncrementByStage = nbPhotos/19;
  } else if (increment >= nbIncrementByStage) {
    stage++;
    increment = 0;

    lcd.setCursor(stage, 3);
    lcd.print("=");
    nbIncrementByStage = nbPhotos/(19-stage);    
  }
}


void releaseShutter() {
  digitalWrite(shutterReleasePin, HIGH);
  delay(70);
  digitalWrite(shutterReleasePin, LOW);
}


// When everything is done, cleanup everything
void cleanup() {
  for (unsigned int i = 0; i < nbTypingZone; i++) 
    delete(typingZoneCollection[i]);
  
  nbPhotos = 0;
  nbSteps = 0;
  interval = 0;
  shutterspeed_100ms = 0;
  
  nbTypingZone = 0;
  noPanning = false;
  shutterspeedFraction = true;
  panningSetupInSteps = true;
  
  Timer1.stop();
  waitCounter100ms = 0;
  
  panning.setCurrentPosition(0); // This is the new Home
}


void initShutterspeedTypingArea(bool firstInit = false) {
  if (!firstInit) {
    delete(typingZoneCollection[nbTypingZone-1]); // We supose here that it's the latest one
    nbTypingZone--;
  }
  lcd.setCursor(15, 0);
  if (shutterspeedFraction) {
    lcd.print("1/   ");    
    shutterspeedTypingArea = initTypingZone(0, 17, 3);
  } else {
    lcd.print("     ");
    shutterspeedTypingArea = initTypingZone(0, 15, 2);
  }
}

void initPanningTypingArea(bool firstInit = false) {
  if (!firstInit) {
    delete(typingZoneCollection[nbTypingZone-1]); // We supose here that it's the latest one
    nbTypingZone--;
  }
  lcd.setCursor(0, 0);
  if (panningSetupInSteps) {
    lcd.print(LCD_MENU_SETUP_PANNING_1);    
    lcd.setCursor(0, 1);
    lcd.print(LCD_MENU_SETUP_PANNING_2);    
    setupPanningTypingArea = initTypingZone(0, 11, 5);
  } else {
    lcd.print(LCD_MENU_SETUP_PANNING_2);    
    lcd.setCursor(17, 0);
    lcd.print(CUSTOM_CHAR_DEGRE);
    lcd.setCursor(0, 1);
    lcd.print(LCD_MENU_SETUP_PANNING_1);
    setupPanningTypingArea = initTypingZone(0, 13, 4);
  }  
}


// Moving from one state to another one
// This is doing all the settings for the new state
void changeState(state newState) {
  state prevState = currentState; // backup the previous state
  currentState = newState;
  
  if (newState == INIT
      || newState == SETUP_NBSHOT 
      || newState == SETUP_PANNING 
      || newState == SETUP_INTERSHOT_DELAY 
      || newState == SETUP_SHUTTER
      || newState == READY_TO_GO
      || (newState == RUNNING_PRE_WAIT && prevState == READY_TO_GO)) {
    printFullScreen();
  }
      
  switch (currentState) {
    case INIT:
      cleanup();
    break;
    
    case SETUP_NBSHOT:
      panning.setCurrentPosition(0); // Here is the new home position
      nbPhotoTypingArea = initTypingZone(0, 12, 6);
    break;
    
    case SETUP_PANNING:
      initPanningTypingArea(true); // True means first init
    break;
    
    case SETUP_INTERSHOT_DELAY:
      intervalTypingArea = initTypingZone(0, 16, 4);
    break;
    
    case SETUP_SHUTTER:
      initShutterspeedTypingArea(true); // True means first init
    break;

    case READY_TO_GO:
      changeTypingArea(-1); // Disable typing area
    break;
    
    case RUNNING_PRE_WAIT:
      refreshLiveData();
      if (prevState == READY_TO_GO) { // First shot
        keyPressed = NO_KEY;
        progressBar(true); // True means init of the progress bar
      } else { // Looping again n again
        progressBar();
        Timer1.stop(); // Need to disable the timer to avoid concurency issue over waitCounter100ms
      }
      waitCounter100ms += (interval*10);
      
      if (waitCounter100ms<=0) { // Check that we are not completely out
        lcd.clear();
        lcd.print("Error: Move time is");
        lcd.setCursor(0, 1);
        lcd.print("longer than the");
        lcd.setCursor(0, 2);
        lcd.print("programmed wait.");
        lcd.setCursor(0, 3);
        lcd.print(waitCounter100ms);
        lcd.print(" (100ms base)");
        while(keypad.getKey() == NO_KEY); // Press any key to go back to main screen
        changeState(INIT);
        return;
      }
      
      if (prevState == READY_TO_GO) // First shot
        Timer1.start();
      else
        Timer1.resume(); // Enable timer
    break;   
    
    case RUNNING_POST_WAIT:
      Timer1.stop(); // Need to disable the timer to avoid concurency issue over waitCounter100ms
      waitCounter100ms += (shutterspeed_100ms) + SHUTTERSPEED_MARGIN_100MS;
      Timer1.resume(); // Enable timer
    break; 
    
    case RUNNING_MOVE:
      panning.enableOutputs();
      panning.move(nbSteps * (panningDirection?1:-1));
    break;
    
    default:
    break;
  }
}


void loop() {
  
  //////////////////////////////
  // Get inputs
  //////////////////////////////
  if (currentState == INIT
      || currentState == SETUP_NBSHOT 
      || currentState == SETUP_PANNING 
      || currentState == SETUP_INTERSHOT_DELAY 
      || currentState == SETUP_SHUTTER
      || currentState == READY_TO_GO) {
    keyPressed = keypad.getKey();
  }

  //////////////////////////////
  // Process
  //////////////////////////////
  if (keyPressed != NO_KEY){
    Serial.print("[Keypad] : ");
    Serial.println(keyPressed);
    
    if (currentState == SETUP_NBSHOT 
        || currentState == SETUP_PANNING 
        || currentState == SETUP_INTERSHOT_DELAY 
        || currentState == SETUP_SHUTTER) {
          
      if (keyPressed == 'A') {
        resetCurrentTypingArea();
        refreshLiveData();
      } else if (keyPressed >= '0' && keyPressed <= '9') {
        processTypingArea(keyPressed);
        refreshLiveData();
      }
    }
  }
  
  switch (currentState) {

    case INIT:
      if (keyPressed == '1' || keyPressed == '3' || keyPressed == '4' || keyPressed == '5' || keyPressed == '6') 
        panning.enableOutputs();
      
      if (keyPressed == '1') // Left
        panning.move(nbStepsInOnePanRevolution);
      if (keyPressed == '3') // Right
        panning.move(-nbStepsInOnePanRevolution);
      if (keyPressed == '4') // Left
        panning.move(100);
      if (keyPressed == '6') // Right
        panning.move(-100);
      if (keyPressed == '5') // Home
        panning.moveTo(0);
      if (keyPressed == '0') // Set the new Home
        panning.setCurrentPosition(0); // Here is the new home position
      if (keyPressed == '*') // Stop
        panning.stop(); 

      // When target is reached
      if (panning.distanceToGo() == 0) { 
        panning.disableOutputs();
        if (keyPressed == 'A')
          noPanning = true;
        if (keyPressed == 'B')
          panningDirection = false;
        if (keyPressed == 'C') 
          panningDirection = true;
        
        if (keyPressed == 'A' || keyPressed == 'B' || keyPressed == 'C') 
          changeState(SETUP_NBSHOT);
      }
    break;
    
    case SETUP_NBSHOT:
      if (keyPressed == '#' && nbPhotos != 0) {
        nbPhotoToTake = nbPhotos;
        if (noPanning)
          changeState(SETUP_INTERSHOT_DELAY);
        else
          changeState(SETUP_PANNING);
      }
    break;
    
    case SETUP_PANNING:
      if (keyPressed == 'C') {  
        panningSetupInSteps = !panningSetupInSteps;
        initPanningTypingArea();
      }
      if (nbSteps != 0) {
        if (keyPressed == 'B') 
          runPanDemo();
        if (keyPressed == '#') 
          changeState(SETUP_INTERSHOT_DELAY);
      }
    break;
     
    case SETUP_INTERSHOT_DELAY:
      if (keyPressed == '#' && interval != 0) {
        if (noPanning)
          changeState(READY_TO_GO);
        else
          changeState(SETUP_SHUTTER);
      }
    break;

    case SETUP_SHUTTER:
      if (keyPressed == 'B') {
        shutterspeedFraction = !shutterspeedFraction;
        initShutterspeedTypingArea();
      } else if (keyPressed == '#') 
        changeState(READY_TO_GO);
    break;
    
    case READY_TO_GO:
      if (keyPressed == '#') 
        changeState(RUNNING_PRE_WAIT);    
    break;
    
    case RUNNING_PRE_WAIT:
      if (waitCounter100ms <= 0)
        changeState(RUNNING_SHUTTER_RELEASE);
    break;

    case RUNNING_SHUTTER_RELEASE:
      releaseShutter();
      nbPhotos--;
      changeState(RUNNING_POST_WAIT);
    break;

    case RUNNING_POST_WAIT:
      if (waitCounter100ms <= 0) {
        if (nbPhotos <= 0) // End
          changeState(INIT);
        else if (noPanning)
          changeState(RUNNING_PRE_WAIT);
        else
          changeState(RUNNING_MOVE);
      }
    break;

    case RUNNING_MOVE:
      if (panning.distanceToGo() == 0) {
        delay(100);
        panning.disableOutputs();
        changeState(RUNNING_PRE_WAIT);
      }
    break;
    
  }
  
  //////////////////////////////
  // Act on outputs
  //////////////////////////////
  panning.run(); // do one step if required
  
}

// Most usefull tasks :

//void AccelStepper::move(long relative)	
//Set the target position relative to the current position

//void AccelStepper::moveTo(long absolut)	
//Set the target position in absolut from the 0

//void AccelStepper::run()	
//Do one step if required to reach the target

//void AccelStepper::runToPosition()	
//Blocking, go to the target, and leave

//void AccelStepper::runToNewPosition(long absolut)
//Blocking, go to the given absolut position, and leave

