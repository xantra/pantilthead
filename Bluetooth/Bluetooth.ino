
// RX  -> D3
// TX  -> D2

#define bluetooth Serial1
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <AccelStepper.h>
#include <TimerOne.h>


#define RECEIVED_MAX_SIZE 200
#define PROTOCOL_VERSION "v2.1"
#define HAS_PANNING 1
#define HAS_TILTING 0
#define HAS_SLIDING 0

#define MOTOR_REFRESH_TIME_US 100 // This is the time in us to configure the timer to refresh the motor
#define WAIT_REFRESH_TIME_US 100000 // This is the time in ms to configure the timer to decrement the wait

#define shutterReleasePin PIN_B0

// Panning Motor pin definitions
#define motorPanPin1  PIN_B1     // IN1 on the ULN2003 driver 1
#define motorPanPin2  PIN_B2     // IN2 on the ULN2003 driver 1
#define motorPanPin3  PIN_B3     // IN3 on the ULN2003 driver 1
#define motorPanPin4  PIN_B4     // IN4 on the ULN2003 driver 1


// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper with 28BYJ-48
AccelStepper panning(AccelStepper::HALF4WIRE, motorPanPin1, motorPanPin3, motorPanPin2, motorPanPin4);

const int CAPABILITIES = HAS_PANNING + HAS_TILTING*2 + HAS_SLIDING*4;

// Buffer to store what is received until the final char \0
char json_received[RECEIVED_MAX_SIZE];
unsigned int nb_char_received = 0;

// JSON structure size : 10 JSON object (key/value pairs)
const int JSON_BUFFER_SIZE = JSON_OBJECT_SIZE(10) + JSON_ARRAY_SIZE(0);


// Board setting, loaded at bootup from eeprom
bool strict_mode = false;
unsigned int oneshot_shutter_hold_delay = 0;
unsigned int nb_steps_for_full_panning = 0;

// Timelapse data
unsigned int nb_shots = 0;
unsigned int intershot_delay = 0;

// Panning data
bool do_panning = false;
int steps_per_shot = 0;
bool move_right = false;
bool stop_for_shot = false;

// Activity flags
bool active_shutter = false;
bool active_wait = false;
bool active_motor = false;

// Processing variables
unsigned int wait_counter;


void write_board_settings() {
  
  EEPROM.update(0, strict_mode ? 1 : 0);
   
  EEPROM.update(1, oneshot_shutter_hold_delay >> 8);
  EEPROM.update(2, oneshot_shutter_hold_delay & 0x00FF);

  EEPROM.update(3, nb_steps_for_full_panning >> 8);
  EEPROM.update(4, nb_steps_for_full_panning & 0x00FF);
  
}

void read_board_settings() {
  
  strict_mode = EEPROM.read(0) ? true : false;
  oneshot_shutter_hold_delay = (EEPROM.read(1) << 8) | EEPROM.read(2);
  nb_steps_for_full_panning = (EEPROM.read(3) << 8) | EEPROM.read(4);
  
}

void setup() {
  //bluetooth.begin(38400); // AT mode (entered by powering the board while pressing the bp on the bt module)
  //Serial.begin(38400);

  bluetooth.begin(9600);
  Serial.begin(9600);
  
  pinMode(shutterReleasePin, OUTPUT);
  digitalWrite(shutterReleasePin, LOW);

  read_board_settings();
  
  Timer1.initialize(MOTOR_REFRESH_TIME_US); // Init the timer 1
  Timer1.stop();
  Timer1.attachInterrupt(interrupt);

  panning.setMaxSpeed(1000.0);
  panning.setAcceleration(500.0); // acceleration/deceleration rate
  panning.runToNewPosition(64); // reset motor position

}

void loop() {
  if (bluetooth.available()) {
    char received = bluetooth.read();
    
    json_received[nb_char_received++] = received;
    
    if (received == '\0') {    
      Serial.println();
      decode_json();
      
//      for (int i = 0; i < nb_char_received; i++) 
//        json_received[i] = '\0';
      nb_char_received = 0;
      
    } else if (nb_char_received >= RECEIVED_MAX_SIZE) {
      Serial.println("Data sent is too long");
      nb_char_received = 0;
    } else {
//      Serial.print(received);
    }
  }
//  if (Serial.available()) {
//    bluetooth.write(Serial.read());
//  }    
    
   if (active_shutter) {
     
    releaseShutter();
    nb_shots--;
    
    active_shutter = false;
    if (nb_shots != 0) {
      active_wait = true;
      wait_counter = intershot_delay * 1000 / (WAIT_REFRESH_TIME_US/1000);
      Timer1.setPeriod(WAIT_REFRESH_TIME_US);
      Timer1.start();
    }

  } else if (active_wait) {
   
    if (wait_counter == 0) {
      Timer1.stop();
      active_wait = false;
      if (do_panning) {
        active_motor = true;
        panning.move(steps_per_shot * (move_right ? -1 : 1));
        Timer1.setPeriod(MOTOR_REFRESH_TIME_US);
        Timer1.start();
      } else 
        active_shutter = true;
    }
    
  } else if (active_motor) {
    
    if (panning.distanceToGo() == 0) {
      Timer1.stop();
      active_motor = false;
      active_shutter = true;
    }
    
  } else if (nb_shots != 0 && intershot_delay != 0) {
    active_shutter = true;
  }

}


void releaseShutter() {
  digitalWrite(shutterReleasePin, HIGH);
  delay(oneshot_shutter_hold_delay);
  digitalWrite(shutterReleasePin, LOW);
}

void interrupt() {
    if (active_motor)
      panning.run(); // do one step if required
    else if (active_wait) 
      wait_counter--;
}

void decode_json() {
  
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;      
  JsonObject& root = jsonBuffer.parseObject(json_received);
  
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return;
  } else {
    Serial.println("prettyPrint : ");
    root.prettyPrintTo(Serial);
    Serial.println();
  }
  
  for (JsonObject::iterator it=root.begin(); it!=root.end(); ++it) {

    if (String("board_settings").equals(it->key)) { 
      receive_board_settings(it->value);
    
    } else if (String("timelapse_setup").equals(it->key)) { 
      receive_timelapse_setup(it->value);
    
    } else if (String("request").equals(it->key)) { 
      receive_request(it->value.asString());
    
    } else {
      // TODO
    }
  }
}


void send_json(JsonObject& root) {
  
  root.printTo(bluetooth);
  bluetooth.print('\0');
  
}


////
//// Board reply
////

void reply_board_settings() {

  StaticJsonBuffer<JSON_OBJECT_SIZE(5)> jsonBuffer;      
  JsonObject& data = jsonBuffer.createObject();
  data["protocol_version"] = PROTOCOL_VERSION;
  data["strict_mode"] = strict_mode; 
  data["capabilities"] = CAPABILITIES;
  data["oneshot_shutter_hold_delay"] = oneshot_shutter_hold_delay;
  if (HAS_PANNING) 
    data["nb_steps_for_full_panning"] = nb_steps_for_full_panning;

  StaticJsonBuffer<JSON_OBJECT_SIZE(1)> jsonBuffer2;      
  JsonObject& root = jsonBuffer2.createObject();
  root["board_settings"] = data;
  
  send_json(root);

}

void reply_timelapse_setup() {

  StaticJsonBuffer<JSON_OBJECT_SIZE(3)> panning_buff;      
  JsonObject& panning = panning_buff.createObject();
   
  StaticJsonBuffer<JSON_OBJECT_SIZE(3)> timelapse_buff;      
  JsonObject& timelapse = timelapse_buff.createObject();
  
  timelapse["nb_shots"] = nb_shots;
  timelapse["intershot_delay"] = intershot_delay;
  if (HAS_PANNING && do_panning) { 
    panning["steps_per_shot"] = steps_per_shot;
    panning["move_right"] = move_right;
    panning["stop_for_shot"] = stop_for_shot;
    
    timelapse["panning"] = panning;
  }
  
  StaticJsonBuffer<JSON_OBJECT_SIZE(1)> root_buff;      
  JsonObject& root = root_buff.createObject();
  root["timelapse_setup"] = timelapse;
  
  send_json(root);
}

void reply_ack(unsigned int return_code, const char* param) {
  
  StaticJsonBuffer<JSON_OBJECT_SIZE(2)> jsonBuffer;      
  JsonObject& data = jsonBuffer.createObject();
  data["id"] = return_code;
  if (param != NULL) 
    data["parameters"] = param; 

  StaticJsonBuffer<JSON_OBJECT_SIZE(1)> jsonBuffer2;      
  JsonObject& root = jsonBuffer2.createObject();
  root["ack"] = data;
  
  send_json(root);
}

////
//// Board receive
////

void receive_board_settings(JsonObject& board_settings) {

  for (JsonObject::iterator it=board_settings.begin(); it!=board_settings.end(); ++it) {

    if (String("protocol_version").equals(it->key)) { // TODO : Need to do something here
      Serial.print("protocol_version : ");
      Serial.println(it->value.asString());
      
    } else if (String("strict_mode").equals(it->key)) { 
      strict_mode = it->value;

    } else if (String("oneshot_shutter_hold_delay").equals(it->key)) { 
      oneshot_shutter_hold_delay = it->value;

    } else if (String("nb_steps_for_full_panning").equals(it->key)) { 
      nb_steps_for_full_panning = it->value;

    } else {
      // TODO
    } 
  }
  
  write_board_settings();
  
  reply_ack(0, NULL);
}

void receive_timelapse_setup(JsonObject& timelapse_setup) {
  
  do_panning = false;
  
  for (JsonObject::iterator it=timelapse_setup.begin(); it!=timelapse_setup.end(); ++it) {

    if (String("nb_shots").equals(it->key)) { 
      nb_shots = it->value;

    } else if (String("intershot_delay").equals(it->key)) { 
      intershot_delay = it->value;

    } else if (HAS_PANNING && String("panning").equals(it->key)) { 
      do_panning = true;
      receive_panning(it->value);

    } else {
      // TODO
    } 
  }
    
  reply_ack(0, NULL);
}

void receive_panning(JsonObject& panning) {
    
  for (JsonObject::iterator it=panning.begin(); it!=panning.end(); ++it) {

    if (String("steps_per_shot").equals(it->key)) { 
      steps_per_shot = it->value;

    } else if (String("move_right").equals(it->key)) { 
      move_right = it->value;

    } else if (String("stop_for_shot").equals(it->key)) { 
      stop_for_shot = it->value;

    } else {
      // TODO
    } 
  }    
}

void receive_request(String request) {

  if (String("all").equals(request)) {
    reply_board_settings();
    reply_timelapse_setup();

  } else if (String("actives").equals(request)) {
    if (active_shutter || active_wait || active_motor)
      reply_timelapse_setup();
    else
      reply_ack(0, NULL); // TODO
    
  } else if (String("board_settings").equals(request)) {
    reply_board_settings();
    
  } else if (String("timelapse_setup").equals(request)) {
    reply_timelapse_setup();
    
  } else {
    // TODO
  }
    
}
